#include <iostream>
#include <list>
#include <stack>
#include <queue>

using namespace std;




class krawedzie{
public:
	int waga; /// waga krawedzi
	int v1;   /// wierzcholek pierwszy
	int v2;		/// wierzcholek drugi

	/**
	* @brief Konstruktor krawedzi.
	*
	* Tworzy krawedz. Obiekt ten jest wykorzystywany przez inna klase graf
	*/
	krawedzie(int, int, int);

};



class graf{
public:
	int numer;
	list<int>lista_wierzcholkow;		///Lista przechowujaca wierzcholki
	stack<int>DFS_stos;					///Stos  do algorytmu DFS
	queue<int>BFS_kolejka;				///Kolejka  do algorytmu BFS
	list<int>uzywane_wierzcholki;		///Lista do reralizacji algorytmow BFS i DFS
	list<int>pozostale_wierzcholki;		///
	list<int>odwiedzone_wierzcholki;	///Lista odwiedzonych wierzcholkow
	list<krawedzie*>lista_krawedzi;     /// Lista krawedzi
	list<krawedzie*>::iterator iter_kraw;  /// Iterator do listy krawedzi
	list<int>::iterator iter_wiersz;		/// Iterator do listy wierzcholkow


	/**
	* @brief dodaj wierzcholek.
	*
	* Dodaje wierzcholek do listy wierzcholkow.
	*/
	void dodaj_wierzcholek(int);

	/**
	* @brief dodaj krawedz.
	*
	* Dodaje krawedz laczaca poszczegolne wierzcholki do listy krawedzi.
	*/
	void dodaj_krawedz(int, int, int);
	/**
	* @brief usun wierzcholek.
	*
	* Usuwa wierzcholek z listy wierzcholkow
	*/
	void usun_wierzcholek(int);
	/**
	* @brief usun krawedz.
	*
	* Usuwa krawedz z listy krawedzi.
	*/
	void usun_krawedz(int, int);
	/**
	* @brief polaczenie wierzcholkow.
	*
	* Funkcja sprawdza czy podane wierzcholki sa polaczone krawedzia.
	*/
	bool czy_polaczone(int, int);
	/**
	* @brief istnienie wierzcholkow.
	*
	* Funkcja sprawdza czy podane wierzcholki znajduja sie w liscie wierzcholkow.
	*/
	bool czy_istnieja_v(int, int);
	/**
	* @brief istnienie wierzcholkow.
	*
	* Funkcja sprawdza czy podane wierzcholki znajduja sie w liscie wierzcholkow.
	*/
	void DFS(int, bool);
	/**
	* @brief Algorytm przeszukiwania w glab.
	*
	* Algorytm odwiedza poszczegolne wierzcholki. Argumentami jest poczatkowy wierzcholek oraz wartosc typu bool, ktora decyduje czy lista odwiedzonych wierzcholkow ma byc wyswietlona.
	*/
	void BFS(int, bool);
	/**
	* @brief Algorytm przeszukiwania w glab.
	*
	* Algorytm odwiedza poszczegolne wierzcholki.
	*/





};

krawedzie::krawedzie(int waga, int v1, int v2)
{
	this->waga = waga;
	this->v1 = v1;
	this->v2 = v2;

}




void graf::dodaj_wierzcholek(int x)
{

	if (!lista_wierzcholkow.empty())

	for (iter_wiersz = lista_wierzcholkow.begin(); iter_wiersz != lista_wierzcholkow.end(); iter_wiersz++)
	{

		if ((*iter_wiersz) == x)
		{
			cout << "Podany wierzcholek juz istnieje! Nie dodano wierzcholka" << endl;
			return;
		}


		else if ((*iter_wiersz) <= 0)
		{
			cout << "Podany wierzcholek musi miec wartosc wieksza od 0" << endl;
			return;
		}
	}

	lista_wierzcholkow.push_back(x);



}

void graf::dodaj_krawedz(int v1, int v2, int waga)
{
	if (!czy_polaczone(v1, v2) && czy_istnieja_v(v1, v2))

	if (v1>v2)
		lista_krawedzi.push_back(new krawedzie(waga, v2, v1));
	else if (v1 < v2)
		lista_krawedzi.push_back(new krawedzie(waga, v1, v2));
	else
		cout << "Podano dwa takie same wierzcholki" << endl;
	else
		cout << "Krawedz laczaca podane wierzcholki juz istnieje lub podane wierzcholki nie istnieja" << endl;
}

void graf::usun_wierzcholek(int x)
{
	for (iter_wiersz = lista_wierzcholkow.begin(); iter_wiersz != lista_wierzcholkow.end(); iter_wiersz++)
	{
		if (*iter_wiersz == x)
		{
			numer = *iter_wiersz;
			lista_wierzcholkow.erase(iter_wiersz);
			break;

		}

	}






}

void graf::usun_krawedz(int v1, int v2)
{
	if (czy_polaczone(v1, v2))
		lista_krawedzi.erase(iter_kraw);
	else
		cout << "Podana krawedz nie istnieje. Nie usunieto krawedzi" << endl;

}


bool graf::czy_istnieja_v(int v1, int v2)
{
	int licz = 0;

	for (iter_wiersz = lista_wierzcholkow.begin(); iter_wiersz != lista_wierzcholkow.end(); iter_wiersz++)
	{
		if (licz == 2)
			return true;

		if ((*iter_wiersz == v1) || (*iter_wiersz == v2))
			licz++;
	}

	return false;
}


bool graf::czy_polaczone(int v1, int v2)
{



	for (iter_kraw = lista_krawedzi.begin(); iter_kraw != lista_krawedzi.end(); iter_kraw++)
	{
		if (((*iter_kraw)->v1 == v1) && ((*iter_kraw)->v2 == v2))
			return true;
	}

	return false;


}





void graf::DFS(int x, bool czy_wyswietlic)
{

	bool odwiedzony_nastepnik;
	bool odwiedzony_wierzcholek;

	odwiedzone_wierzcholki.clear();
	uzywane_wierzcholki.clear();

	odwiedzone_wierzcholki.push_back(x);

	do {

		if (!DFS_stos.empty())
			DFS_stos.pop();


		for (iter_kraw = lista_krawedzi.begin(); iter_kraw != lista_krawedzi.end(); iter_kraw++)
		{
			if ((*iter_kraw)->v2 == x)
			{
				odwiedzony_nastepnik = false;
				odwiedzony_wierzcholek = false;

				for (iter_wiersz = uzywane_wierzcholki.begin(); iter_wiersz != uzywane_wierzcholki.end(); iter_wiersz++)
				{

					if (*iter_wiersz == (*iter_kraw)->v1)
						odwiedzony_nastepnik = true;

					if (*iter_wiersz == (*iter_kraw)->v2)
						odwiedzony_wierzcholek = true;

				}

				if (!odwiedzony_wierzcholek)
					uzywane_wierzcholki.push_back((*iter_kraw)->v2);
				if (!odwiedzony_nastepnik)
					DFS_stos.push((*iter_kraw)->v1);
				uzywane_wierzcholki.push_back((*iter_kraw)->v1);


			}

			if ((*iter_kraw)->v1 == x)
			{
				odwiedzony_nastepnik = false;
				odwiedzony_wierzcholek = false;

				for (iter_wiersz = uzywane_wierzcholki.begin(); iter_wiersz != uzywane_wierzcholki.end(); iter_wiersz++)
				{

					if (*iter_wiersz == (*iter_kraw)->v2)
						odwiedzony_nastepnik = true;

					if (*iter_wiersz == (*iter_kraw)->v1)
						odwiedzony_wierzcholek = true;

				}

				if (!odwiedzony_wierzcholek)
					uzywane_wierzcholki.push_back((*iter_kraw)->v1);
				if (!odwiedzony_nastepnik)
				{
					DFS_stos.push((*iter_kraw)->v2);
					uzywane_wierzcholki.push_back((*iter_kraw)->v2);
				}





			}



		}

		if (!DFS_stos.empty())
		{
			x = DFS_stos.top();
			odwiedzone_wierzcholki.push_back(x);
		}


	} while (!DFS_stos.empty());




	if (czy_wyswietlic)
	for (iter_wiersz = odwiedzone_wierzcholki.begin(); iter_wiersz != odwiedzone_wierzcholki.end(); iter_wiersz++)
	{
		cout << *iter_wiersz << endl;
	}


}


void graf::BFS(int x, bool czy_wyswietlic)
{



	bool odwiedzony_nastepnik;
	bool odwiedzony_wierzcholek;

	odwiedzone_wierzcholki.clear();
	uzywane_wierzcholki.clear();

	odwiedzone_wierzcholki.push_back(x);

	do {

		if (!BFS_kolejka.empty())
			BFS_kolejka.pop();


		for (iter_kraw = lista_krawedzi.begin(); iter_kraw != lista_krawedzi.end(); iter_kraw++)
		{
			if ((*iter_kraw)->v2 == x)
			{
				odwiedzony_nastepnik = false;
				odwiedzony_wierzcholek = false;

				for (iter_wiersz = uzywane_wierzcholki.begin(); iter_wiersz != uzywane_wierzcholki.end(); iter_wiersz++)
				{

					if (*iter_wiersz == (*iter_kraw)->v1)
						odwiedzony_nastepnik = true;

					if (*iter_wiersz == (*iter_kraw)->v2)
						odwiedzony_wierzcholek = true;

				}

				if (!odwiedzony_wierzcholek)
					uzywane_wierzcholki.push_back((*iter_kraw)->v2);
				if (!odwiedzony_nastepnik)
					BFS_kolejka.push((*iter_kraw)->v1);
				uzywane_wierzcholki.push_back((*iter_kraw)->v1);


			}

			if ((*iter_kraw)->v1 == x)
			{
				odwiedzony_nastepnik = false;
				odwiedzony_wierzcholek = false;

				for (iter_wiersz = uzywane_wierzcholki.begin(); iter_wiersz != uzywane_wierzcholki.end(); iter_wiersz++)
				{

					if (*iter_wiersz == (*iter_kraw)->v2)
						odwiedzony_nastepnik = true;

					if (*iter_wiersz == (*iter_kraw)->v1)
						odwiedzony_wierzcholek = true;

				}

				if (!odwiedzony_wierzcholek)
					uzywane_wierzcholki.push_back((*iter_kraw)->v1);
				if (!odwiedzony_nastepnik)
				{
					BFS_kolejka.push((*iter_kraw)->v2);
					uzywane_wierzcholki.push_back((*iter_kraw)->v2);
				}





			}



		}

		if (!BFS_kolejka.empty())
		{
			x = BFS_kolejka.front();
			odwiedzone_wierzcholki.push_back(x);
		}


	} while (!BFS_kolejka.empty());



	if (czy_wyswietlic)
	for (iter_wiersz = odwiedzone_wierzcholki.begin(); iter_wiersz != odwiedzone_wierzcholki.end(); iter_wiersz++)
	{
		cout << *iter_wiersz << endl;
	}


}

int main(void)
{

	graf Graf;

	Graf.dodaj_wierzcholek(1);
	Graf.dodaj_wierzcholek(2);
	Graf.dodaj_wierzcholek(3);
	Graf.dodaj_wierzcholek(4);
	Graf.dodaj_wierzcholek(5);
	Graf.dodaj_wierzcholek(6);



	Graf.dodaj_krawedz(1, 2, 6);
	Graf.dodaj_krawedz(1, 4, 4);
	Graf.dodaj_krawedz(2, 5, 5);
	Graf.dodaj_krawedz(3, 2, 5);






	Graf.DFS(1, true);
	Graf.BFS(1, true);



	system("Pause");
	return 0;
}