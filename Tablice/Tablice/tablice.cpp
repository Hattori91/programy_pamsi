﻿#include <iostream>
#include <fstream>
#include <cstdlib>
#include <time.h>

using namespace std;




class Tablica{

public:


	int *tab;
	int wielkosc;
	Tablica(char*);


	friend ostream & operator<< (ostream&, Tablica&);			//Przeciazenie operatora wyjscia
	Tablica& operator= (Tablica&);								//Przeciazenie operatora przypisania
	bool operator== (Tablica&);									//Przeciazenie operatora porownania


};





class Metody{

public:

	void Wyswietl(Tablica);
	void Zamien_Elementy(Tablica, int, int);
	void Odwroc_Tablice(Tablica);
	void Dodaj_Element(Tablica, int);



};

Tablica::Tablica(char *nazwa)										//Konstruktor
{
	int i;
	fstream plik;													//Operacje na pliku
	plik.open(nazwa, std::ios::in | std::ios::out);				//Dostep do pliku plik.txt
	if (plik.good() == true)										//Sprawdzenie czy plik zostal poprawnie otwarty
	{
		cout << "Uzyskano dostep do pliku: " << nazwa << endl;

		plik >> wielkosc;

		tab = new int[wielkosc];									//Alokacja pamieci

		for (i = 0; i<wielkosc; i++)
			plik >> tab[i];										//Zainicjowanie kolejnych komorek tabeli							

		plik.close();											//Zakonczenie operacji na plikach

	}

	else
	{

		cout << "Dostep do pliku zostal zabroniony!" << endl;		//W przypadku bledu otwarcia pliku plik.txt
		exit(1);													//Konczenie programu
	}

}


ostream & operator<< (ostream &wyjscie, Tablica &wartosc)
{
	for (int i = 0; i<wartosc.wielkosc; i++)
		wyjscie << wartosc.tab[i] << " ";
	return wyjscie;
}

Tablica& Tablica::operator= (Tablica &tab1)							//Przeciazenie operatora przypisania
{

	for (int i = 0; i<wielkosc; i++)
	{
		tab[i] = tab1.tab[i];
	}


	return *this;
}


bool Tablica::operator== (Tablica &tab1)							//Przeciazenie operatora porownania
{

	for (int i = 0; i<wielkosc; i++)
	{
		if (tab[i] != tab1.tab[i])
			return false;
	}


	return true;
}



void Metody::Odwroc_Tablice(Tablica obiekt)
{

	int i;

	int *tym;

	tym = new int[obiekt.wielkosc];


	for (i = 0; i<obiekt.wielkosc; i++)
	{
		tym[i] = obiekt.tab[obiekt.wielkosc - 1 - i];

	}

	for (i = 0; i<obiekt.wielkosc; i++)
	{
		obiekt.tab[i] = tym[i];

	}

	delete[] tym;														//Usuwanie tablicy z pamieci


}



void Metody::Dodaj_Element(Tablica obiekt, int element)
{
	int *pom;															//Wskaznik pomocniczy 

	obiekt.wielkosc = obiekt.wielkosc + 1;

	pom = obiekt.tab;													//Przekazanie adresu do wskaznika pomocniczego

	obiekt.tab = new int[obiekt.wielkosc];								//Alokacja nowej pamieci dla tablicy powiekszonej o jedna komorke

	for (int i = 0; i<obiekt.wielkosc - 1; i++)								//Przepisanie wartosci do tablicy
		obiekt.tab[i] = pom[i];

	obiekt.tab[obiekt.wielkosc - 1] = element;							//Przepisanie argumentu funkcji do ostatniej komorki tablicy

	delete[] pom;														//Usuwanie zbednego obszaru pamieci



}




void Metody::Wyswietl(Tablica obiekt)
{

	for (int i = 0; i<obiekt.wielkosc; i++)
		cout << obiekt.tab[i] << " ";										//Wyswitlanie wszystkich elementow tablicy

	cout << endl;

}

void Metody::Zamien_Elementy(Tablica obiekt, int a, int b)					//Zamiana elementow tablicy
{
	int pom;

	pom = obiekt.tab[a];

	obiekt.tab[a] = obiekt.tab[b];

	obiekt.tab[b] = pom;



}





int main(void)
{
	clock_t start, koniec;
	long czas;


	start = clock();													//Start odliczania czasu

	Metody Obiekt;													//Tworzenie obiektu 
	Tablica Tablica1("Plik.txt");									//Tworzenie tablicy z pliku.txt
	Tablica Tablica2("Plik.txt");									//Tworzenie tablicy z pliku.txt	
	Obiekt.Odwroc_Tablice(Tablica2);

	cout << Tablica1 << endl;
	cout << Tablica2 << endl;
	Tablica1 = Tablica2;
	cout << Tablica1 << endl;

	if (Tablica1 == Tablica2)
		cout << "TRUE" << endl;
	else
		cout << "FALSE" << endl;

	koniec = clock();													//Koniec odliczania
	czas = (long)(koniec - start);									//czas dzia�a� w ms
	cout << "Czas wykonywania: " << czas << "ms" << endl;
	system("Pause");


	return 0;
}